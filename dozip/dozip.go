package dozip

import (
	"archive/zip"
	"io"
	"os"
	"path"
	"path/filepath"
	"strings"
)

//压缩一个或多个文件
func FilesToZip(destZip string, srcFiles ...string) error {
	zipfile, err := os.Create(destZip)
	if err != nil {
		return err
	}
	defer zipfile.Close()

	writer := zip.NewWriter(zipfile)
	defer writer.Close()

	for _, file := range srcFiles {
		f, err := os.Open(file)
		if err != nil {
			return err
		}
		fileWriter, err := writer.Create(f.Name())

		_, err = io.Copy(fileWriter, f)
		if err != nil {
			return err
		}
		_ = f.Close()
	}

	return nil
}

//压缩目录
func DirToZip(destZip string, srcDir string) error {
	zipfile, err := os.Create(destZip)
	if err != nil {
		return err
	}
	defer zipfile.Close()

	archive := zip.NewWriter(zipfile)
	defer archive.Close()

	filepath.Walk(srcDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		header, err := zip.FileInfoHeader(info)
		if err != nil {
			return err
		}

		header.Name = strings.TrimPrefix(path, filepath.Dir(srcDir)+"/")

		if info.IsDir() {
			header.Name += "/"
		} else {
			header.Method = zip.Deflate
		}

		writer, err := archive.CreateHeader(header)
		if err != nil {
			return err
		}

		if !info.IsDir() {
			file, err := os.Open(path)
			if err != nil {
				return err
			}
			defer file.Close()
			_, err = io.Copy(writer, file)
		}
		return err
	})

	return nil
}

//解压
type UnZipOptions struct {
	//不解压目录
	NotUnzipDir bool
	//可以解压的最大文件限制（byte）
	//当超过这个限制，该文件将被跳过解压
	MaxFileSize int64
}
func Unzip(srcZip, destDir string, opts *UnZipOptions) error {
	zipReader, err := zip.OpenReader(srcZip)
	if err != nil {
		return err
	}
	defer zipReader.Close()

	for _, file := range zipReader.File {
		if opts.NotUnzipDir && path.Dir(file.Name) != "." {
			continue
		}
		fpath := filepath.Join(destDir, file.Name)
		if file.FileInfo().IsDir() {
			os.MkdirAll(fpath, os.ModePerm)
		} else {
			if  opts.MaxFileSize > 0 && file.FileInfo().Size() > opts.MaxFileSize {
				continue
			}
			if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
				return err
			}
			f, err := file.Open()
			if err != nil {
				return err
			}
			defer f.Close()
			destFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, file.Mode())
			if err != nil {
				return err
			}
			defer destFile.Close()

			_, err = io.Copy(destFile, f)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
