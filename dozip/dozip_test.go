package dozip

import "testing"

func TestDirToZip(t *testing.T) {
	type args struct {
		destZip string
		srcDir  string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "多文件压缩", args: args{destZip: "./destzip.zip", srcDir: "srcFile"},wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := DirToZip(tt.args.destZip, tt.args.srcDir); (err != nil) != tt.wantErr {
				t.Errorf("DirToZip() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestFilesToZip(t *testing.T) {
	type args struct {
		destZip  string
		srcFiles []string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{name: "多文件压缩", args: args{destZip: "./destzip2.zip", srcFiles: []string{"./srcFile/x.abc","dozip.go"}},wantErr: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := FilesToZip(tt.args.destZip, tt.args.srcFiles...); (err != nil) != tt.wantErr {
				t.Errorf("FilesToZip() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUnzip(t *testing.T) {
	type args struct {
		srcZip  string
		destDir string
		opts    *UnZipOptions
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		//解压目录
		{name: "解压", args: args{srcZip: "./destzip2.zip", destDir: "./dest", opts: &UnZipOptions{}}},
		//不解压目录
		{name: "解压", args: args{srcZip: "./destzip2.zip", destDir: "./dest", opts: &UnZipOptions{NotUnzipDir: true}}},
		//限制最大解压文件
		{name: "解压", args: args{srcZip: "./destzip2.zip", destDir: "./dest", opts: &UnZipOptions{MaxFileSize: 3500}}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Unzip(tt.args.srcZip, tt.args.destDir, tt.args.opts); (err != nil) != tt.wantErr {
				t.Errorf("Unzip() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
