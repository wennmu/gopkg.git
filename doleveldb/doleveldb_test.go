package doleveldb

import (
	"context"
	"testing"
)

func TestDoleveldb(t *testing.T) {
	//初始化
	ctx, _ := context.WithCancel(context.Background())
	err := NewDoleveldb(ctx, ".sys/data")
	if err != nil {
		t.Errorf(err.Error())
	}

	//新增key
	err = Put("foo", "bar")
	if err != nil {
		t.Errorf(err.Error())
	}
	t.Log("success Put.")

	//获取key
	res, err := Get("foo")
	if err != nil {
		t.Errorf(err.Error())
	}
	t.Log("Get foo: ", res)

	//删除key
	err = Delete("foo")
	if err != nil {
		t.Errorf(err.Error())
	}
	t.Log("success Delete.")

	//再次获取key
	res, err = Get("foo")
	if err != nil {
		t.Errorf(err.Error())
	}

	//输出结果:
	//doleveldb_test.go:18: success Put.
	//doleveldb_test.go:23: Get foo:  bar
	//doleveldb_test.go:28: success Delete.
	//doleveldb_test.go:31: leveldb: not found
}
