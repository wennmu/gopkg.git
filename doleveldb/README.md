# Doleveldb 搞定文件存储DB

> _基于goleveldb封装的键值数据库_

#### 使用说明

```go

//初始化
ctx, _ := context.WithCancel(context.Background())
err := NewDoleveldb(ctx, ".sys/data")
if err != nil {
    t.Errorf(err.Error())
}

//新增key
err = Put("foo", "bar")
if err != nil {
    t.Errorf(err.Error())
}
t.Log("success Put.")

//获取key
res, err := Get("foo")
if err != nil {
    t.Errorf(err.Error())
}
t.Log("Get foo: ", res)

//删除key
err = Delete("foo")
if err != nil {
    t.Errorf(err.Error())
}
t.Log("success Delete.")

//如果需要获取DB
doleveldb.DB
```

#### 测试示例

```shell
参阅 doleveldb_test.go
```