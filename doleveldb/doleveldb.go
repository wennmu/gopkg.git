package doleveldb

import (
	"context"
	"log"

	"github.com/syndtr/goleveldb/leveldb"
)

var (
	DB *leveldb.DB
)

//初始化leveldb
func NewDoleveldb(ctx context.Context, path string) error {
	newCtx, _ := context.WithCancel(ctx)
	dbClient, err := leveldb.OpenFile(path, nil)
	if err != nil {
		return err
	}
	DB = dbClient
	go func() {
		select {
		case <-newCtx.Done():
			_ = DB.Close()
			log.Printf("db closed")
		}
	}()

	return nil
}

//新增key
func Put(key, val string) error {
	if err := DB.Put([]byte(key), []byte(val), nil); err != nil {
		return err
	}
	return nil
}

//获取key
func Get(key string) (string, error) {
	data, err := DB.Get([]byte(key), nil)
	return string(data), err
}

//删除key
func Delete(key string) error {
	if err := DB.Delete([]byte(key), nil); err != nil {
		return err
	}
	return nil
}
