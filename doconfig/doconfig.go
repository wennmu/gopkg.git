//注意配置优先级，自上而下，优先级由高到低
//explicit call to Set
//flag
//env
//config
//key/value store
//default

package doconfig

import (
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"log"
	"time"
)

//读取默认的配置文件
//配置文件位于./.sys/config.yaml
func InitConfigs() error {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("config/")
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Println("config file not found")
			return err
		} else {
			log.Fatalf("read config file error. err: %s", err.Error())
			return err
		}
	}
	return nil
}

//加载配置文件
//支持指定多个路径
func LoadConfigs(configName string, configType string, configPath string) error {
	viper.SetConfigName(configName)
	viper.SetConfigType(configType)
	viper.AddConfigPath(configPath)
	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			log.Fatalln("config file not found")
			return err
		} else {
			log.Fatalf("read config file error. err: %s", err.Error())
			return err
		}
	}
	return nil
}

//绑定环境变量到配置
func BindEnvConfig(prefix string, key string) error {
	viper.SetEnvPrefix(prefix)
	return viper.BindEnv(key)
}

//绑定flag到配置
func BindPflagConfig(pflagKey string, cmdFlag *pflag.Flag) {
	viper.BindPFlag(pflagKey, cmdFlag)
}

//保存配置到文件
func WriteConfig() {
	viper.WriteConfig()
}

//保存配置到文件
func SafeWriteConfig() {
	viper.SafeWriteConfig()
}

//保存配置到指定文件
func WriteConfigAs(filePath string) {
	viper.WriteConfigAs(filePath)
}

//保存配置到指定文件
func SafeWriteConfigAs(filePath string) {
	viper.SafeWriteConfigAs(filePath)
}

//设置配置
func SetDefault(key string, val interface{}) {
	viper.SetDefault(key, val)
}

func Set(key string, val interface{}) {
	viper.Set(key, val)
}

//读取配置
func Get(key string) interface{} {
	return viper.Get(key)
}

func GetBool(key string) bool {
	return viper.GetBool(key)
}

func GetFloat64(key string) float64 {
	return viper.GetFloat64(key)
}

func GetInt(key string) int {
	return viper.GetInt(key)
}

func GetIntSlice(key string) []int {
	return viper.GetIntSlice(key)
}

func GetString(key string) string {
	return viper.GetString(key)
}

func GetStringMap(key string) map[string]interface{} {
	return viper.GetStringMap(key)
}

func GetStringMapString(key string) map[string]string {
	return viper.GetStringMapString(key)
}

func GetStringSlice(key string) []string {
	return viper.GetStringSlice(key)
}

func GetTime(key string) time.Time {
	return viper.GetTime(key)
}

func GetDuration(key string) time.Duration {
	return viper.GetDuration(key)
}

func IsSet(key string) bool {
	return viper.IsSet(key)
}

func AllSettings() map[string]interface{} {
	return viper.AllSettings()
}

//获取配置实例
//可以直接调用配置库提供的方法
func GetInstance() *viper.Viper {
	return viper.GetViper()
}