package doconfig

import (
	"fmt"
	"testing"
)

//初始化默认配置文件
func TestInitConigs(t *testing.T) {
	if err := InitConfigs(); err != nil {
		t.Log(err.Error())
	}

	//读取配置
	fmt.Println(Get("DNS"))
	fmt.Println(Get("db.0.mysql"))
	fmt.Println(GetStringMap("db.0"))
	fmt.Println(GetStringSlice("menu"))
	fmt.Println(GetInt("page"))
	fmt.Println(GetFloat64("score"))
	fmt.Println(GetIntSlice("intslice"))
	fmt.Println(GetBool("state"))
	fmt.Println(GetTime("createAt"))
	fmt.Println(GetDuration("createAt"))
	fmt.Println(IsSet("createAt"))
	fmt.Println(AllSettings())

	//绑定环境变量配置
	if err := BindEnvConfig("test", "secret"); err != nil {
		t.Log(err.Error())
	}
	fmt.Println(Get("secret"))
}

//加载指定配置
func TestLoadConfigs(t *testing.T) {
	if err := LoadConfigs("config", "yaml", "config"); err != nil {
		t.Log(err.Error())
	}
	//读取配置
	fmt.Println(Get("name"))
}

//使用GetInstance实例
//并且写入配置文件
func TestGetInstance(t *testing.T) {
	//加载指定配置
	if err := LoadConfigs("config", "yaml", "config"); err != nil {
		t.Log(err.Error())
	}
	//读取配置
	fmt.Println(Get("name"))

	//使用GetInstance实例
	GetInstance().Set("year", "2022")

	Set("status", "on")

	//写入配置文件
	//覆盖原来的配置文件，不仅仅覆盖值
	//WriteConfig()

	//写入到一个文件中
	//覆盖原来的配置文件，不仅仅覆盖值
	WriteConfigAs("config/config.yaml")

	//写入到一个文件中
	//不会覆盖原来的文件
	//SafeWriteConfig()

	//写入到一个文件中
	//不会覆盖原来的文件
	//SafeWriteConfigAs("other.yaml")
}
