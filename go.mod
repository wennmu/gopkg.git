module gitee.com/wennmu/gopkg.git

go 1.16

require (
	github.com/aws/aws-sdk-go v1.43.34 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/spf13/viper v1.11.0 // indirect
	github.com/syndtr/goleveldb v1.0.0 // indirect
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df
)
