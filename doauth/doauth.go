package doauth

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// 全局秘钥
var AppSecret = ""

// 生成token
func GenToken(uid int64) (token string, expire interface{}, err error) {
	atCliams := jwt.MapClaims{}

	atCliams["authorized"] = true
	atCliams["uid"] = uid
	atCliams["exp"] = time.Now().Add(time.Hour * 24).Unix()

	at := jwt.NewWithClaims(jwt.SigningMethodHS256, atCliams)
	token, err = at.SignedString([]byte(AppSecret))
	if err != nil {
		return "", "", err
	}

	return token, atCliams["exp"], nil
}

// 验证token
func VerifyToken(bearToken string) (uid int64, err error) {
	var tokenStr string
	tokenSlice := strings.Split(bearToken, " ")
	tokenStr = tokenSlice[1]
	// tokenStr = c.Query("token")
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return uid, errors.New("unexpected signing method")
		}
		return []byte(AppSecret), nil
	})
	if err != nil {
		return uid, errors.New("Illegal token")
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if ok && token.Valid {
		uid, err = strconv.ParseInt(fmt.Sprintf("%.f", claims["uid"]), 10, 64)
		if err != nil || uid <= 0 {
			return uid, errors.New("Illegal user")
		}
	}
	return uid, nil
}
