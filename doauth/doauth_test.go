package doauth

import "testing"

func TestGenToken(t *testing.T) {
	type args struct {
		uid int64
	}
	tests := []struct {
		name       string
		args       args
		wantToken  string
		wantExpire string
		wantErr    bool
	}{
		{name: "gen token", args: args{
			uid: 1,
		}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotToken, gotExpire, err := GenToken(tt.args.uid)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotToken != tt.wantToken {
				t.Errorf("GenToken() gotToken = %v, want %v", gotToken, tt.wantToken)
			}
			if gotExpire != tt.wantExpire {
				t.Errorf("GenToken() gotExpire = %v, want %v", gotExpire, tt.wantExpire)
			}
		})
	}
}

func TestVerifyToken(t *testing.T) {
	type args struct {
		bearToken string
	}
	tests := []struct {
		name    string
		args    args
		wantUid int64
		wantErr bool
	}{
		{
			name: "verify token",
			args: args{
				bearToken: "bear eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkIjp0cnVlLCJleHAiOjE2NDkzMDAzMzYsInVpZCI6MX0.7qsIzYr6TTfLk_64CCN13SPeFL8n3c-C9PziEYBMCLw",
			},
			wantUid: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotUid, err := VerifyToken(tt.args.bearToken)
			if (err != nil) != tt.wantErr {
				t.Errorf("VerifyToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotUid != tt.wantUid {
				t.Errorf("VerifyToken() gotUid = %v, want %v", gotUid, tt.wantUid)
			}
		})
	}
}
