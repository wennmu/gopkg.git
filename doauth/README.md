# Doauth 搞定鉴权

#### 介绍

 _提供给予jwt的token的生成和token验证_ 



> 支持生成token
> 
> 支持验证token
> 


#### 安装教程


```
go get gitee.com/wennmu/pkg
```


#### 使用说明

```go
// 设置APPSECRET
doauth.APPSECRET = "XXX"

// 调用生成token函数
gotToken, gotExpire, err := doauth.GenToken(tt.args.uid)


// 调用验证token函数
gotUid, err := doauth.VerifyToken(tt.args.bearToken)
```


#### 参阅测试

```shell
./doauth_test.go
```
