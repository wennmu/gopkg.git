package doredis

import "github.com/go-redis/redis/v8"

var rdb *redis.Client

//使用默认选项初始化
func InitDefault(addr, passwrod string)  {
	rdb = redis.NewClient(&redis.Options{
		Addr:     addr,
		Password: passwrod,
		DB:       0,
	})
}

//使用自定义选项初始化
func InitServer(opts *redis.Options)  {
	rdb = redis.NewClient(opts)
}

//获取rdb实例
func GetRDB() *redis.Client  {
	return rdb
}