package doredis

import (
	"context"
	"testing"
)

func TestRdb(t *testing.T) {
	var ctx = context.Background()
	//使用默认初始化
	InitDefault("localhost:6379", "")

	//获取实例操作数据
	err := GetRDB().Set(ctx, "key", "value", 0).Err()
	if err != nil {
		t.Error(err)
	}

	val, err := GetRDB().Get(ctx, "key").Result()
	if err != nil {
		t.Error(err)
	}
	t.Log("key: ", val)
}
