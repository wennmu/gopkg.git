package doorm

import (
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"time"
)

var GlobalDB *gorm.DB

type GlobalDBOptions struct {
	//日志模式
	LogMode bool
	Dialector  gorm.Dialector
	//数据库地址
	DSN string
	//自定义日志，需要实现logger.Interface
	//参考https://gorm.io/zh_CN/docs/logger.html
	Logger logger.Interface
	//连接池参数
	MaxIdleConns int
	MaxOpenConns int
	ConnMaxLifetime time.Duration
}

//初始化mysql客戶端
func InitMysqlDB(opts GlobalDBOptions) error {
	db, err := gorm.Open(mysql.Open(opts.DSN), &gorm.Config{Logger: opts.Logger})
	if err != nil {
		return err
	}
	GlobalDB = db
	return nil
}

//初始化mysql客戶端，运用了连接池
func InitMysqlDBWithConns(opts GlobalDBOptions) error {
	db, err := gorm.Open(mysql.Open(opts.DSN), &gorm.Config{Logger: opts.Logger})
	if err != nil {
		return err
	}
	GlobalDB = db

	//开启连接池
	// 获取通用数据库对象 sql.DB ，然后使用其提供的功能
	sqlDB, err := db.DB()
	if err != nil {
		return err
	}
	// SetMaxIdleConns 用于设置连接池中空闲连接的最大数量。
	sqlDB.SetMaxIdleConns(opts.MaxIdleConns)

	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	sqlDB.SetMaxOpenConns(opts.MaxOpenConns)

	// SetConnMaxLifetime 设置了连接可复用的最大时间。
	sqlDB.SetConnMaxLifetime(opts.ConnMaxLifetime)

	return nil
}

//获取db实例
//比起直接使用GlobalDB更加优雅
func DB() *gorm.DB {
	return GlobalDB
}

//关闭DB
//在主程序退出的时候记得关闭数据库链接
func Close() error {
	sqlDB, err := GlobalDB.DB()
	if err != nil {
		return err
	}
	return sqlDB.Close()
}