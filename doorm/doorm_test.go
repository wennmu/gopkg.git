package doorm

import (
	"fmt"
	"log"
	"os"
	"testing"
	"time"

	"gitee.com/wennmu/gopkg.git/dologger"
	"gorm.io/gorm/logger"
)

var DSN = ""

func TestOrm(t *testing.T) {
	//初始化日志
	dologger.InitLogger(&dologger.LoggerOptions{
		LogDebug:     true,
		LogPath:      "log/test-access.log",
		LogErrorPath: "log/test-error.log",
	})
	//初始化
	if err := InitMysqlDBWithConns(GlobalDBOptions{
		LogMode: true,
		DSN:     DSN,
		Logger: NewLogger(
			log.New(os.Stdout, "\r\n", log.LstdFlags), // io writer（日志输出的目标，前缀和日志包含的内容——译者注）
			logger.Config{
				SlowThreshold:             time.Second,   // 慢 SQL 阈值
				LogLevel:                  logger.Silent, // 日志级别
				IgnoreRecordNotFoundError: true,          // 忽略ErrRecordNotFound（记录未找到）错误
				Colorful:                  false,         // 禁用彩色打印
			},
		),
		MaxIdleConns:    10,
		MaxOpenConns:    100,
		ConnMaxLifetime: time.Hour,
	}); err != nil {
		panic(err)
	}
	//关闭链接
	defer Close()

	//查询数据
	type data struct {
		Id int
	}
	var d data
	dbimpl := DB().Table("config").First(&d)
	if dbimpl.Error != nil {
		fmt.Println(dbimpl.Error.Error())
	}
	fmt.Println("id is", d.Id)
}
