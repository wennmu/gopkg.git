# DoLogger 搞定日志

## 如何使用

```go
//在服务开始的时候初始化日志
InitLogger(&LoggerOptions{
    LogDebug: true,
    LogPath: "log/test-access.log",
    LogErrorPath: "log/test-error.log",
})

//在任何地方调用日志
Debugf(ctx, fmt.Sprintf("debug msg:%v", errors.New("params error.")))
Infof(ctx, fmt.Sprintf("info msg:%v", errors.New("params error.")))
Warnf(ctx, fmt.Sprintf("warm msg:%v", errors.New("params error.")))
Errorf(ctx, fmt.Sprintf("error msg:%v", errors.New("params error.")))
Logf(1, fmt.Sprintf("error msg:%v", errors.New("params error.")))

//在任何地方调用日志
Debug("params error.")
Info("params error.")
Warn("params error.")
Error("params error.")
Log(1,"params error.")

//获取实例
GetLogger().Log(1, "failed.") //level=fatal
GetLogger().Log(2, "failed.") //level=error
GetLogger().Log(3, "failed.") //level=warning
GetLogger().Log(4, "failed.") //level=info
GetLogger().Log(5, "failed.") //level=debug
```

## 测试示例

```shell
dologger_test.go
```