package dologger

import (
	"context"
	rotatelogs "github.com/lestrrat-go/file-rotatelogs"
	"github.com/rifflock/lfshook"
	"github.com/sirupsen/logrus"
	"time"
)

type Logger struct {
}

type LoggerOptions struct {
	//开启Debug
	LogDebug bool
	//日志路径
	LogPath string
	//错误日志路径
	LogErrorPath string
	//文件名后缀格式,例如:"%Y%m%d"
	PathFormat string
	//分割时间，例如:24
	RotationTime uint
}

var GlobalLogger *logrus.Logger

//初始化日志
//在服务开始的时候进行初始化
func InitLogger(options *LoggerOptions) {
	if options.PathFormat == "" {
		options.PathFormat = ".%Y%m%d"
	}
	if options.RotationTime <= 0 {
		options.RotationTime = 24
	}
	getWriter := func(path string) (*rotatelogs.RotateLogs, error) {
		return rotatelogs.New(
			path+options.PathFormat,
			rotatelogs.WithLinkName(path),
			rotatelogs.WithRotationTime(time.Duration(options.RotationTime)*time.Hour),
		)
	}
	logWriter, err := getWriter(options.LogPath)
	if err != nil {
		logrus.Fatalf("Unable to create log, msg: %v", err)
	}

	logErrorWriter, err := getWriter(options.LogErrorPath)
	if err != nil {
		logrus.Fatalf("Unable to create error log,  msg: %v", err)
	}

	pathMap := lfshook.WriterMap{
		logrus.DebugLevel: logWriter,
		logrus.InfoLevel:  logWriter,
		logrus.WarnLevel:  logWriter,
		logrus.ErrorLevel: logErrorWriter,
	}

	GlobalLogger = logrus.New()

	if options.LogDebug {
		GlobalLogger.SetLevel(logrus.DebugLevel)
	}

	GlobalLogger.Hooks.Add(lfshook.NewHook(
		pathMap,
		&logrus.TextFormatter{},
	))
}

func Debugf(ctx context.Context, args ...interface{}) {
	requestId := ctx.Value("requestId")
	GlobalLogger.Debugf("requestId:%s, msg: %+v", requestId, args)
}

func Infof(ctx context.Context, args ...interface{}) {
	requestId := ctx.Value("requestId")
	GlobalLogger.Infof("requestId:%s, msg: %+v", requestId, args)
}

func Warnf(ctx context.Context, args ...interface{}) {
	requestId := ctx.Value("requestId")
	GlobalLogger.Warnf("requestId:%s, msg: %+v", requestId, args)
}

func Errorf(ctx context.Context, args ...interface{}) {
	requestId := ctx.Value("requestId")
	GlobalLogger.Errorf("requestId:%s, msg: %+v", requestId, args)
}

func Logf(level logrus.Level, format string, args ...interface{}) {
	GlobalLogger.Logf(level, format, args...)
}

func Debug(args ...interface{}) {
	GlobalLogger.Debug(args...)
}

func Info(args ...interface{}) {
	GlobalLogger.Info(args...)
}

func Warn(args ...interface{}) {
	GlobalLogger.Warn(args...)
}

func Error(args ...interface{}) {
	GlobalLogger.Error(args...)
}

func Log(level logrus.Level, args ...interface{}) {
	GlobalLogger.Log(level, args...)
}

//获取logger实例，直接使用原方法
func GetLogger() *logrus.Logger {
	return GlobalLogger
}
