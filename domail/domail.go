package domail

import (
	"fmt"
	"gopkg.in/gomail.v2"
	"math/rand"
	"time"
)

/*函数部分*/
/*如果这里满足需求，尽量使用这里的函数*/

//发送文本邮件
func SendMail(addr string, port int, username, password string, from *From, receives []Receive, subject, body string) error {
	s, err := NewDomail(addr, port, username, password)
	if err != nil {
		return err
	}
	for _, to := range receives {
		err = s.SetHeader(from, to.Addr, subject).SetTextBody(body).Send()
		if err != nil {
			return err
		}
	}

	return nil
}

//发送html邮件
func SendHtmlMail(addr string, port int, username, password string, from *From, receives []Receive, subject, body string) error {
	s, err := NewDomail(addr, port, username, password)
	if err != nil {
		return err
	}
	for _, to := range receives {
		err = s.SetHeader(from, to.Addr, subject).SetHtmlBody(body).Send()
		if err != nil {
			return err
		}
	}

	return nil
}

//发送携带附件的文本邮件
func SendMailWithAttach(addr string, port int, username, password string, from *From, receives []Receive, subject, body, filepath string) error {
	s, err := NewDomail(addr, port, username, password)
	if err != nil {
		return err
	}
	for _, to := range receives {
		err = s.SetHeader(from, to.Addr, subject).SetTextBody(body).SetAttach(filepath).Send()
		if err != nil {
			return err
		}
	}

	return nil
}

//发送携带附件的HTML邮件
func SendHtmlMailWithAttach(addr string, port int, username, password string, from *From, receives []Receive, subject, body, filepath string) error {
	s, err := NewDomail(addr, port, username, password)
	if err != nil {
		return err
	}
	for _, to := range receives {
		err = s.SetHeader(from, to.Addr, subject).SetHtmlBody(body).SetAttach(filepath).Send()
		if err != nil {
			return err
		}
	}

	return nil
}

//发送邮件验证码
//这里默认6位随机数字
//body模板"您好，验证码是%s"
func SendMailCode(addr string, port int, username, password string, from *From, receives []Receive, subject, body string) (string, error) {
	code := fmt.Sprintf("%06v", rand.New(rand.NewSource(time.Now().UnixNano())).Int31n(1000000))
	err := SendMail(addr, port, username, password, from, receives, subject, fmt.Sprintf(body,code))
	if err != nil {
		return code, err
	}
	return code, nil
}

/*封装部分*/
/*这里对gomail进行了封装*/

//如果函数部分没办法满足需求，可以直接使用该部分代码
type From struct {
	Name string
	Addr string 
}

type Receive struct {
	Name string
	Addr string
}

type Domail struct {
	M *gomail.Message
	D gomail.SendCloser
}

func NewDomail(addr string, port int, username, password string) (*Domail, error) {
	var s Domail
	d, err := gomail.NewDialer(addr, port, username, password).Dial()
	if err != nil {
		return &s, err
	}
	s.D = d
	s.M = gomail.NewMessage()
	return &s, nil
}

func (s *Domail) SetHeader(from *From, to, subject string) *Domail {
	s.M.SetAddressHeader("From", from.Addr, from.Name)
	s.M.SetHeader("To", to)
	s.M.SetHeader("Subject", subject)
	return s
}

func (s *Domail) SetTextBody(body string) *Domail {
	s.M.SetBody("text/plain", body)
	return s
}

func (s *Domail) SetHtmlBody(body string) *Domail {
	s.M.SetBody("text/html", body)

	return s
}

func (s *Domail) SetAttach(filepath string) *Domail {
	s.M.Attach(filepath)

	return s
}

func (s *Domail) SetAttachWithSetting(filepath string, filesetting gomail.FileSetting) *Domail {
	s.M.Attach(filepath, filesetting)

	return s
}

func (s *Domail) Send() error {
	if err := gomail.Send(s.D, s.M); err != nil {
		return err
	}
	s.M.Reset()
	return nil
}
