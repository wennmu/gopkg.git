# Domail 搞定邮件

#### 介绍

 _基于gomail实现多场景封装，只需按需调用即可_ 



> 支持发送文本邮件
> 
> 支持发送文本带附件邮件
> 
> 支持发送html邮件
> 
> 支持发送html带附件邮件
> 
> 支持发送邮件验证码
> 
> 支持自定义函数发送邮件


#### 安装教程


```
go get gitee.com/wennmu/pkg
```


#### 使用说明

```go
package main

import (
	"gitee.com/wennmu/pkg.git/domail"
	"log"
)

func main() {
	code, err := domail.SendMailCode("smtp.gmail.com", 587, "xxx@gmail.com", "***", &domail.From{Name: "每日菜菜", Addr: "xxx@gmail.com"}, []domail.Receive{{Name: "", Addr: "yyy@outlook.com"},}, "重要操作验证码", "您好，您的验证码是%s，使用后请尽快销毁。")
	if err != nil {
		log.Fatalln("err: ", err)
	}
	log.Println("code: ",code)
}

```





