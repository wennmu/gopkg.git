package domail

import (
	"testing"
)

func Test_sendHtmlMail(t *testing.T) {
	type args struct {
		addr     string
		port     int
		username string
		password string
		from     *From
		receives []Receive
		subject  string
		body     string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SendHtmlMail(tt.args.addr, tt.args.port, tt.args.username, tt.args.password, tt.args.from, tt.args.receives, tt.args.subject, tt.args.body); (err != nil) != tt.wantErr {
				t.Errorf("sendHtmlMail() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_sendHtmlMailWithAttach(t *testing.T) {
	type args struct {
		addr     string
		port     int
		username string
		password string
		from     *From
		receives []Receive
		subject  string
		body     string
		filepath string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SendHtmlMailWithAttach(tt.args.addr, tt.args.port, tt.args.username, tt.args.password, tt.args.from, tt.args.receives, tt.args.subject, tt.args.body, tt.args.filepath); (err != nil) != tt.wantErr {
				t.Errorf("sendHtmlMailWithAttach() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_sendMail(t *testing.T) {
	type args struct {
		addr     string
		port     int
		username string
		password string
		from     *From
		receives []Receive
		subject  string
		body     string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "发送文本邮件",
			args: args{
				addr:     "smtp.gmail.com",
				port:     587,
				username: "xxx@gmail.com",
				password: "***",
				from:     &From{Name: "BOb", Addr: "xxx@gmail.com"},
				receives: []Receive{
					{Name: "", Addr: "yyy@outlook.com"},
				},
				subject: "来自幸福的邀请",
				body:    "一起去喝一杯下午茶怎么样？",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SendMail(tt.args.addr, tt.args.port, tt.args.username, tt.args.password, tt.args.from, tt.args.receives, tt.args.subject, tt.args.body); (err != nil) != tt.wantErr {
				t.Errorf("sendMail() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_sendMailCode(t *testing.T) {
	type args struct {
		addr     string
		port     int
		username string
		password string
		from     *From
		receives []Receive
		subject  string
		body     string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "发送邮件验证码",
			args: args{
				addr:     "smtp.gmail.com",
				port:     587,
				username: "xxx@gmail.com",
				password: "***",
				from:     &From{Name: "每日菜菜", Addr: "xxx@gmail.com"},
				receives: []Receive{
					{Name: "", Addr: "yyy@outlook.com"},
				},
				subject: "重要操作验证码",
				body:    "您好，您的验证码是%s，使用后请销毁。",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := SendMailCode(tt.args.addr, tt.args.port, tt.args.username, tt.args.password, tt.args.from, tt.args.receives, tt.args.subject, tt.args.body)
			t.Log("sendMailCode() got = %v, want %v", got, err)
		})
	}
}

func Test_sendMailWithAttach(t *testing.T) {
	type args struct {
		addr     string
		port     int
		username string
		password string
		from     *From
		receives []Receive
		subject  string
		body     string
		filepath string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SendMailWithAttach(tt.args.addr, tt.args.port, tt.args.username, tt.args.password, tt.args.from, tt.args.receives, tt.args.subject, tt.args.body, tt.args.filepath); (err != nil) != tt.wantErr {
				t.Errorf("sendMailWithAttach() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
