package doreuqest

import (
	"io/ioutil"
	"net/http"
	"strings"
)

//发起get请求
func Get(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

//发起post请求
func Post(url string, params string) (string, error) {
	resp, err := http.Post(url, "application/json", strings.NewReader(params))
	if err != nil {
		return "", err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}

//发起请求
//可指定请求方式
//可进行basic auth 验证
func Request(method, url, params string, username, password string) (string, error) {
	client := &http.Client{}

	req, err := http.NewRequest(method, url, strings.NewReader(params))
	if err != nil {
		return "", err
	}

	req.Header.Set("Content-Type", "application/json; charset=UTF-8")
	if username != "" {
		req.SetBasicAuth(username, password)
	}

	resp, err := client.Do(req)

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	return string(body), nil
}
