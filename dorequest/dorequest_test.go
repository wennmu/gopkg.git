package doreuqest

import (
	"encoding/json"
	"testing"
)

func TestGet(t *testing.T) {
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "get",
			args: args{
				url: "https://jsonplaceholder.typicode.com/posts",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Get(tt.args.url)
			if (err != nil) != tt.wantErr {
				t.Errorf("Get() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Get() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPost(t *testing.T) {
	params, _ := json.Marshal(map[string]string{
		"name": "test",
	})
	type args struct {
		url    string
		params string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "post",
			args: args{
				url:    "https://jsonplaceholder.typicode.com/posts",
				params: string(params),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Post(tt.args.url, tt.args.params)
			if (err != nil) != tt.wantErr {
				t.Errorf("Post() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Post() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRequest(t *testing.T) {
	params, _ := json.Marshal(map[string]interface{}{
		"title":  "foo",
		"body":   "bar",
		"userId": 1,
	})
	type args struct {
		method   string
		url      string
		params   string
		username string
		password string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "post",
			args: args{
				method:   "POST", //GET 必须大写
				url:      "https://jsonplaceholder.typicode.com/posts",
				params:   string(params),
				username: "",
				password: "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Request(tt.args.method, tt.args.url, tt.args.params, tt.args.username, tt.args.password)
			if (err != nil) != tt.wantErr {
				t.Errorf("Request() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Request() got = %v, want %v", got, tt.want)
			}
		})
	}
}
