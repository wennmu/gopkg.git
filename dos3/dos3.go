//基于aws s3 sdk实现对minio对象存储的上传、下载、列出、删除操作
package dos3

import (
	"context"
	"crypto/tls"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"io"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	//全局S3对象
	GlobalS3 *S3
	//请求超时时间
	Timeout  = 30 * time.Minute
)

type S3 struct {
	EndPoint  string
	Region    string
	AccessKey string
	SecretKey string
	Sess      *session.Session
}

func Init(endPoint, region, accessKey, secretKey string) error {
	sess, err := session.NewSession(&aws.Config{
		Endpoint:         aws.String(endPoint),
		Region:           aws.String(region),
		S3ForcePathStyle: aws.Bool(true),
		Credentials:      credentials.NewStaticCredentials(accessKey, secretKey, ""),
		HTTPClient:       &http.Client{Transport: &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: true}}},
	})
	if err != nil {
		return err
	}
	GlobalS3 = &S3{
		EndPoint:  endPoint,
		Region:    region,
		AccessKey: accessKey,
		SecretKey: secretKey,
		Sess:      sess,
	}
	return nil
}

func UploadFile(bucket string, fp *os.File) (key string, err error) {
	return GlobalS3.PutObj(bucket, fp)
}

func ListFile(bucket string) ([]*s3.Object, error) {
	return GlobalS3.ListObj(bucket)
}

func DeleteFile(bucket, key string) error {
	return GlobalS3.DelObj(bucket, key)
}

func DownloadFile(bucket, key string) (io.ReadCloser, error) {
	return GlobalS3.DownObj(bucket, key)
}

//列出所有对象
func (s *S3) ListObj(bucket string) ([]*s3.Object, error) {
	ctx, cancelFn := context.WithTimeout(context.Background(), Timeout)
	if cancelFn != nil {
		defer cancelFn()
	}
	svc := s3.New(GlobalS3.Sess)
	objs, err := svc.ListObjectsV2WithContext(ctx, &s3.ListObjectsV2Input{
		Bucket: aws.String(bucket),
	})

	return objs.Contents, err
}

//上传对象
func (s *S3) PutObj(bucket string, fp *os.File) (key string, err error) {
	ctx, cancelFn := context.WithTimeout(context.Background(), Timeout)
	if cancelFn != nil {
		defer cancelFn()
	}
	svc := s3.New(GlobalS3.Sess)
	_, err = svc.PutObjectWithContext(ctx, &s3.PutObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(fp.Name()),
		Body:   fp,
	})
	if err != nil {
		log.Fatalf("put obj err: %s", err.Error())
		return key, err
	}
	return key, nil
}

//下载对象
func (s *S3) DownObj(bucket, key string) (io.ReadCloser, error) {
	ctx, cancelFn := context.WithTimeout(context.Background(), Timeout)
	if cancelFn != nil {
		defer cancelFn()
	}
	svc := s3.New(GlobalS3.Sess)
	obj, err := svc.GetObjectWithContext(ctx, &s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})

	return obj.Body, err
}

//删除对象
func (s *S3) DelObj(bucket, key string) error {
	ctx, cancelFn := context.WithTimeout(context.Background(), Timeout)
	if cancelFn != nil {
		defer cancelFn()
	}
	svc := s3.New(GlobalS3.Sess)
	_, err := svc.DeleteObjectWithContext(ctx, &s3.DeleteObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})

	return err
}
