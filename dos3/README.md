# Dos3 搞定文件上传

### 如何使用


##### 在项目开始调用初始化
```go
if err := Init("http://127.0.0.1:9000", "default", "minioadmin", "minioadmin"); err != nil {
}
```

##### 使用函数操作文件
```go

func UploadFile(bucket string, fp *os.File) (key string, err error) {
return GlobalS3.PutObj(bucket, fp)
}

func ListFile(bucket string) ([]*s3.Object, error) {
return GlobalS3.ListObj(bucket)
}

func DeleteFile(bucket, key string) error {
return GlobalS3.DelObj(bucket, key)
}

func DownloadFile(bucket, key string) (io.ReadCloser, error) {
return GlobalS3.DownObj(bucket, key)
}
```

##### 使用GlobalS3自定义函数
```go

var (
//全局S3对象
GlobalS3 *S3
//请求超时时间
Timeout  = 30 * time.Minute
)

type S3 struct {
EndPoint  string
Region    string
AccessKey string
SecretKey string
Sess      *session.Session
}
```

### 测试示例

```go
参阅测试示例
```

### 遇到问题

```go
请提交issue
```