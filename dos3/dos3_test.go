package dos3

import (
	"io"
	"os"
	"reflect"
	"testing"

	"github.com/aws/aws-sdk-go/service/s3"
)

func TestDeleteFile(t *testing.T) {
	if err := Init("http://127.0.0.1:9000", "default", "minioadmin", "minioadmin"); err != nil {
		t.Errorf(err.Error())
	}
	type args struct {
		bucket string
		key    string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "删除文件",
			args: args{bucket: "test", key: "dos3.go"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := DeleteFile(tt.args.bucket, tt.args.key); (err != nil) != tt.wantErr {
				t.Errorf("DeleteFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestDownloadFile(t *testing.T) {
	if err := Init("http://127.0.0.1:9000", "default", "minioadmin", "minioadmin"); err != nil {
		t.Errorf(err.Error())
	}
	type args struct {
		bucket string
		key    string
	}
	tests := []struct {
		name    string
		args    args
		want    *io.ReadCloser
		wantErr bool
	}{
		{
			name: "下载文件",
			args: args{bucket: "test", key: "dos3.go"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := DownloadFile(tt.args.bucket, tt.args.key)
			if (err != nil) != tt.wantErr {
				t.Errorf("DownloadFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			bytes, _ := io.ReadAll(got)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DownloadFile() got = %v, want %v", string(bytes), tt.want)
			}
		})
	}
}

func TestListFile(t *testing.T) {
	if err := Init("http://127.0.0.1:9000", "default", "minioadmin", "minioadmin"); err != nil {
		t.Errorf(err.Error())
	}
	type args struct {
		bucket string
	}
	tests := []struct {
		name    string
		args    args
		want    []*s3.Object
		wantErr bool
	}{
		{
			name: "列出文件",
			args: args{bucket: "test"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ListFile(tt.args.bucket)
			if (err != nil) != tt.wantErr {
				t.Errorf("ListFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ListFile() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUploadFile(t *testing.T) {
	if err := Init("http://127.0.0.1:9000", "default", "minioadmin", "minioadmin"); err != nil {
		t.Errorf(err.Error())
	}
	f, _ := os.Open("./dos3.go")
	type args struct {
		bucket string
		fp     *os.File
	}
	tests := []struct {
		name    string
		args    args
		wantKey string
		wantErr bool
	}{
		{
			name: "上传文件",
			args: args{bucket: "test", fp: f},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotKey, err := UploadFile(tt.args.bucket, tt.args.fp)
			if (err != nil) != tt.wantErr {
				t.Errorf("UploadFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if gotKey != tt.wantKey {
				t.Errorf("UploadFile() gotKey = %v, want %v", gotKey, tt.wantKey)
			}
		})
	}
}
